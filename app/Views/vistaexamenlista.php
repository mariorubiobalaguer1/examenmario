<html lang="es">
    <head>
        <title>Listado de solicitudes</title>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap5.css"/>
         <script type="text/javascript" src="https://code.jquery.com/jquery-3.6.0.js"></script>
   <script type="text/javascript" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap5.js"></script>
       
      
    </head>
    <body>
        <table id="listilla">
           
            <thead><th>DNI</th>
            <th>Nombre</th>
            <th>Apellido Solicitante1</th>
            <th>Apellido Solicitante2</th>
            <th>Email</th>
            <th>Ciclo</th>
            <th>Matrícula</th>
            <th>Eliminar</th>
           
</thead>
            
            <tbody>
                <?php foreach ($lista as $list): ?>
                <tr>
                    <td><?= $list['nif'] ?></td>
                    <td><?= $list['nombre'] ?></td>
                    <td><?= $list['apellido1'] ?></td> 
                    <td><?= $list['apellido2'] ?></td>
                    <td><?= $list['email'] ?></td>
                    <td><?= $list['nombre'] ?></td>
                    <td><?= $list['tipo_tasa'] ?></td>
                    <td><button></button></td>
                     
                </tr>
                    
            <?php endforeach; ?>


        </tbody>
    </table>    
       
        <script type="text/javascript">
            $(document).ready(function() {
    $('#listilla').DataTable();
} );
            </script>
          <body>
        <br>
        <h2 style="text-align: center;">DATOS DEL ALUMNO</h2>
        <div class="container" id="alineacion">
            <form method="post" action="">
                <div class="form-group" style="width:450px;">
                    <i class="fas fa-id-card-alt"></i>&nbsp;<label name="nif" >DNI</label>
                    <abbr title="Introduce tu DNI"><input type="text" class="form-control" name="nia" id="dni" placeholder="Insertar DNI" pattern="[0-9]{8}[A-Za-z]{1}" title="Ejemplo (12345678X)" required></abbr>
                </div> <br>
                <div class="form-group" style="width:450px;">
                    <i class="fa fa-user bigicon"></i>&nbsp;<label name="nombre">Nombre</label>
                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Insertar Nombre" required>
                </div> <br>
                <div class="form-group" style="width:450px;">
                    <i class="fa fa-user bigicon"></i>&nbsp;<label name="apellido1">Primer Apellido</label>
                    <input type="text" class="form-control" id="apellido1" name="apellido1" placeholder="Insertar Primer Apellido" required>
                </div> <br>
                <div class="form-group" style="width:450px;">
                    <i class="fa fa-user bigicon"></i>&nbsp;<label name="apellido2">Segundo Apellido</label>
                    <input type="text" class="form-control" id="apellido2" name="apellido2" placeholder="Insertar Segundo Apellido" required>
                </div> <br>
                <div class="form-group" style="width:450px;">
                    <i class="fa fa-baby"></i>&nbsp;<label name="nacimiento">Fecha de nacimiento</label>
                    <abbr title="Pon tu fecha de nacimiento "><input type="text" class="form-control" id="nacimiento" placeholder="Insertar fecha de nacimiento " maxlength="12" name="nacimiento" required></abbr>
                </div> <br>
                </div> <br>
                <div class="form-group" style="width:450px;">
                    <i class="fas fa-envelope-square"></i>&nbsp;<label name="email">Dirección de email</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Insertar tu email" required>
                </div> <br>
              <div class="form-group" style="width:450px;">
                    <i class="fas fa-envelope-square"></i>&nbsp;<label name="email">Tipo de matricula</label><select name="Matrícula" required>
   <option value="1">Gratis</option> 
   <option value="2">Medio Financiada</option> 
   <option value="3">Financiada</option> 
                    </select></div>
                <div>
  <input type="radio" id="ordinaria" name="ordinaria" value="ordinaria"
         >
  <label for="ordinaria">Ordinaria</label>
</div>
                <div>
  <input type="radio" id="semi-ordinaria" name="semi-ordinaria" value="semi-ordinaria"
         >
  <label for="semi-ordinaria">Semi-ordinaria</label>
</div>
                <div>
  <input type="radio" id="gratuita" name="gratuita" value="gratuita"
         >
  <label for="gratuita">Gratuita</label>
</div>
                <button type="submit" class="btn btn-secondary" style="margin-left:46%;"><i class="fas fa-upload"></i>&nbsp; Enviar</button>
            </form>
</body>
</html>

!--<!-- El ejercicio 4 he hecho esto: Vamos en la carpeta Config, y ahí vamos a App.php. Buscamos la línea que pone:
 public $defaultLocale = 'en'; y lo cambiamos a  public $defaultLocale = 'es'; -->