<html lang="es">
<head>
<title>Listar todos los grupos</title>
        <style>
            table, td, th, tr {
                border-collapse: collapse;
                border: 3px solid gray;
            }
            tr:nth-child(even) {
                background-color: #dddddd;
            }
            table{
                margin: 0 auto;
            }
            caption{
                font-size: 50px;
            }
            centro {
                margin: 0 auto;
            } 
        </style>
    </head>
    <body>
        <div class="centro">
            <table id="table">
                <caption>Lista de grupos</caption>
                <th>ID</th>
                <th>Codigo</th>
                <th>Nombre</th>

                <tbody>
                    <!--Lee la base de datos  -->
                    <?php foreach ($grupos as $grupo): ?>

                        <!-- Indicamos los datos que vamos a mostrar -->
                        <tr>
                            <td class="bottom"><?= $grupo['id'] ?></td>
                            <td class="bottom">
                                <a href="http://localhost:8080/codeigniter/index.php/controllerGrupos/grupoyalumnos/<?= $grupo['codigo'] ?>">
                                    <?= $grupo['codigo'] ?>
                                </a>    
                            </td>
                            <td class="bottom"><?= $grupo['nombre'] ?></td>

                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </body>
</html>