<!-- Vista que muestra los alumnos pertenecientes a un grupo en concreto que es 1CFSS -->

<html lang="es">
    <head>
        <title>Listar todos los alumnos</title>
        <style>
            table, td, th, tr {
                border-collapse: collapse;
                border: 3px solid gray;
            }
            tr:nth-child(even) {
                background-color: #dddddd;
            }
            table{
                margin: 0 auto;
            }
            caption{
                font-size: 50px;
            }
            centro {
                margin: 0 auto;
            } 
        </style>

    </head>
    <body>
        <div class="centro">
            <table id="table">
                <caption> LISTA DE ALUMNOS </caption>     
                <th>Nombre</th>
                <th>Apellido1</th>
                <th>Apellido2</th>
                <th>EMAIL</th>
                <th>GRUPO</th>
               
                <tbody>
                    <!-- #Lee la base de datos --> 
                    <?php foreach ($alumnos as $alumno): ?>
                        <!-- #Indicamos los datos que vamos a mostrar -->
                        <tr>
                            <td class="bottom"><?= $alumno['nombre'] ?></td>
                            <td class="bottom"><?= $alumno['apellido1'] ?></td>
                            <td class="bottom"><?= $alumno['apellido2'] ?></td>
                            <td class="bottom"><?= $alumno['email'] ?></td>
                            <td class="bottom"><?= $alumno['grupo'] ?></td>       
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>      
        </div>
    </body>
</html> 
