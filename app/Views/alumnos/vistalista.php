<html lang="es">
    <head>
        <title>Listar todos los grupos</title>
        <style>
            table, td, th, tr {
                border-collapse: collapse;
                border: 3px solid gray;
            }
            tr:nth-child(even) {
                background-color: #dddddd;
            }
            table{
                margin: 0 auto;
            }
            caption{
                font-size: 50px;
            }
            centro {
                margin: 0 auto;
            } 
        </style>
    </head>
    <body>
        <table>
            <caption>Lista </caption>
            <th>Nombre</th>
            <th>Apellido 1</th>
            <th>Apellido 2</th>
            <th>Email</th>
            <th>Foto</th>

            <tbody>
                <?php foreach ($alumnos as $alumno): ?>
                <tr>
                    <td><?= $alumno['nombre'] ?></td>
                    <td><?= $alumno['apellido1'] ?></td> 
                    <td><?= $alumno['apellido2'] ?></td>
                    <td><?= $alumno['email'] ?></td>
                    <td><img style="width: 50px"  src="<?= base_url('Fotos/10000/' . sprintf('%06s', $alumno['id'])) ?>.jpg" ></td>
                     
                </tr>
                    
            <?php endforeach; ?>


        </tbody>
    </table>    

</body>
</html>

