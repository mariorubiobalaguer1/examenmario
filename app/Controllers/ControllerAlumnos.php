<?php

namespace App\Controllers;

use App\Models\ModelAlumnos;

class ControllerAlumnos extends BaseController {

    public function index() {
        $alumnos = new ModelAlumnos();
        $data['alumnos'] = $alumnos->findAll();
        echo view('alumnos/vistaalumnos', $data);
    }
    
}
