<?php
namespace App\Controllers ;
use CodeIgniter\Controller;
use App\Models\ModelAlumnos; 
use App\Models\ModelGrupos; 

class ControllerGrupos extends BaseController {

  public function index()
   {
      $grupos = New ModelGrupos();
      $data['grupos'] = $grupos->findAll();
      echo view ('alumnos/vistagrupos', $data);
   }
   
   public function grupoyalumnos($valor="1CFSS") {
        $alumnos = new ModelAlumnos();
        $datos ['alumnos']=$alumnos->select('alumnos.nombre,apellido1,apellido2,email,matricula.grupo,alumnos.id')
        ->join('matricula','alumnos.NIA=matricula.NIA','left')
        ->where(['grupo'=> $valor])
        ->findAll();
        echo view('alumnos/vistalista', $datos);
    }
}